using System;
using System.Net.Http;
using System.Text;
using funCentralGuestHistory.Model;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace funCentralGuestHistory
{
    public static class Function1  
    {
        [FunctionName("Function1")]
        public async static void Run([ServiceBusTrigger("inqueue-centralguesthistory", Connection = "ServiceEndpoint")]string myQueueItem, ILogger log)
        {
            log.LogInformation($"C# ServiceBus queue trigger function processed message: {myQueueItem}");
            using (HttpClient client = new HttpClient())
            {
              


                CentralGuestProfileRoot Model = new CentralGuestProfileRoot();
                Model= JsonConvert.DeserializeObject<CentralGuestProfileRoot>(myQueueItem);
                var _DHCentalGuestHistoryAPI = Environment.GetEnvironmentVariable("DHCentalGuestHistoryAPI");
                var _servicebusconnectionstring = Environment.GetEnvironmentVariable("ServiceEndpoint");

                var _azureAccountName = Environment.GetEnvironmentVariable("AzureAccountName");
                var _azureStorageKey = Environment.GetEnvironmentVariable("AzureStorageKey");



                var token = client.PostAsJsonAsync<CentralGuestProfileRoot>(_DHCentalGuestHistoryAPI, Model).Result;                
                var details = token.Content.ReadAsStringAsync().Result;

                Response _response = new Response();
                _response = JsonConvert.DeserializeObject<Response>(details);
                FXFDCentralGuestData _objCentralGuestdata = new FXFDCentralGuestData();

                try
                {
                    
                    _objCentralGuestdata.PartitionKey = Guid.NewGuid().ToString();
                    _objCentralGuestdata.RowKey = Model.EchoToken;
                    _objCentralGuestdata = await Azuretableoperation.GetCentalGuestDataMapping("FXFDCentralGuestData", _azureAccountName, _azureStorageKey, Model.EchoToken);
                    _objCentralGuestdata.MessageStatus = _response.message;

                   
                  
                }
                catch (Exception)
                {

                    throw;
                }

                await Azuretableoperation.UpdateCentralGuestStatus( _azureAccountName, _azureStorageKey, "FXFDCentralGuestData", _objCentralGuestdata);

                log.LogInformation($"ServiceBus queue trigger function processed message: {details}");
            };

        }
    }
}
