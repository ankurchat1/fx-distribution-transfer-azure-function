﻿using System;
using System.Collections.Generic;
using System.Text;

namespace funCentralGuestHistory.Model
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class ProfileData
    {
        public string Code { get; set; }
        public string PartnerGuestCode { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZIPCODE { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Nationality { get; set; }
        public string DOB { get; set; }
        public string DOA { get; set; }
        public string PassportNo { get; set; }
        public string SpouseName { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string ComAddress { get; set; }
        public string ComCity { get; set; }
        public string ComState { get; set; }
        public string ComCountry { get; set; }
        public string ComZIPCODE { get; set; }
        public string ComEmail { get; set; }
        public string ComPhone { get; set; }
        public string Remarks { get; set; }
        public string GuestStatus { get; set; }
        public string GuestClassification { get; set; }
        public string GuestDesignation { get; set; }
        public string Mobile { get; set; }
        public string GuestCNIC { get; set; }
        public string GDPRInfo { get; set; }
        public string CompanySalesTax { get; set; }
        public string CompanyNTN { get; set; }
        public string Salutation { get; set; }
        public string AlternateEmail { get; set; }
        public string Smoking { get; set; }
        public string IsGSTRegistered { get; set; }
        public string FrequentFlyer { get; set; }
        public string LoyaltyNumber { get; set; }
        public string IsReservationLetterAllowed { get; set; }
        public string IsReservationPhoneAllowed { get; set; }
        public string IsReservationMailAllowed { get; set; }
        public string IsReservationSMSAllowed { get; set; }
        public string IsInvoiceLetterAllowed { get; set; }
        public string IsInvoicePhoneAllowed { get; set; }
        public string IsInvoiceMailAllowed { get; set; }
        public string IsInvoiceSMSAllowed { get; set; }
        public string IsAdvtLetterAllowed { get; set; }
        public string IsAdvtPhoneAllowed { get; set; }
        public string IsAdvtMailAllowed { get; set; }
        public string IsAdvtSMSAllowed { get; set; }
        public string IsMarketingNResearch { get; set; }
        public string IsReceiveThirdPartInfo { get; set; }
        public string IsOptForLoyalty { get; set; }
        public List<GuestAddress> GuestAddress { get; set; }
        public List<GuestDocument> GuestDocument { get; set; }
        public List<GuestAllergy> GuestAllergy { get; set; }
        public List<GuestNote> GuestNote { get; set; }
        public List<GuestDislike> GuestDislike { get; set; }
        public List<GuestPreference> GuestPreference { get; set; }
        public List<GuestLoyaltyCard> GuestLoyaltyCard { get; set; }
        public List<GuestLinkedProfile> GuestLinkedProfile { get; set; }
        public List<GuestComplaint> GuestComplaint { get; set; }
        public List<GuestCreditCard> GuestCreditCard { get; set; }
        public List<GuestDependant> GuestDependants { get; set; }
        public List<GuestVisitDetail> GuestVisitDetails { get; set; }
    }

    public class CentralGuestProfileRoot
    {
        public int PropertyCode { get; set; }
        public string EchoToken { get; set; }
        public string ProfileType { get; set; }
        public string IsFirstTime { get; set; }
        public ProfileData ProfileData { get; set; }
    }


    public class FXFDCentralGuestData
    {
        public string PartitionKey { get; set; } = Guid.NewGuid().ToString();
        public string RowKey { get; set; }
        public string FXCRSGuestCode { get; set; }
        public string ThirdPartyGuestCode { get; set; }
        public string GuestName { get; set; }
        public string PropertyCode { get; set; }
        public string FXFDMessage { get; set; }
        public string ProcessTime { get; set; }
        public string MessageStatus { get; set; }

    }


    public class CentralGuestProfile
    {

        public string PartitionKey { get; set; } = Guid.NewGuid().ToString();
        public string RowKey { get; set; } = Guid.NewGuid().ToString();
        public string CentalGuestRequestJson { get; set; }
        public string GuestProcessRow { get; set; }
        public string FXGuestCode { get; set; }
    }


    public class GuestVisitDetail
    {
        public string GuestCode { get; set; }
        public string RegistrationNo { get; set; }
        public string ArrivalDate { get; set; }
        public string DepartureDate { get; set; }
        public string RoomNo { get; set; }
        public string NetAmount { get; set; }
        public string SettlementMode { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string ComapanyCity { get; set; }
        public string CompanyState { get; set; }
        public string CompanyCountry { get; set; }
        public string CompanyZip { get; set; }
        public string CompanyTelephone { get; set; }
        public string CompanyFax { get; set; }
        public string CompanyEmail { get; set; }
        public string RoomType { get; set; }
        public string RateCode { get; set; }
        public string MarketSegment { get; set; }
        public string BusinessSource { get; set; }
        public string RoomRevenue { get; set; }
        public string FNBRevenue { get; set; }
        public string OtherRevenue { get; set; }
        public string SpecialInstruction { get; set; }
        public int FeedbackRating { get; set; }
        public string FeedbackText { get; set; }
        public string FeedbackType { get; set; }
        public int RoomNights { get; set; }
        public string VisaNumber { get; set; }
        public string VisaDate { get; set; }
        public string VisaPlace { get; set; }
        public string VisaExpiryDate { get; set; }
        public int RecordType { get; set; }
    }

    public class GuestAddress
    {
        public string AddressType { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Extension { get; set; }
    }

    public class GuestDocument
    {
        public string DocumentTypeCode { get; set; }
        public string IssuePlace { get; set; }
        public string IssueDate { get; set; }
        public string ExpiryDate { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
    }

    public class GuestAllergy
    {
        public string Allergy { get; set; }
    }

    public class GuestNote
    {
        public string NoteTypeCode { get; set; }
        public string NoteTitle { get; set; }
        public string NoteDetail { get; set; }
    }

    public class GuestDislike
    {
        public string DislikeCode { get; set; }
        public string DislikeNote { get; set; }
    }

    public class GuestPreference
    {
        public string PreferenceCode { get; set; }
        public string PreferenceDetailList { get; set; }
    }

    public class GuestLoyaltyCard
    {
        public string MembershipTypeCode { get; set; }
        public string MembershipClassCode { get; set; }
        public string MembershipNumber { get; set; }
        public string ExpiryDate { get; set; }
    }

    public class GuestLinkedProfile
    {
        public string LinkedGuestCode { get; set; }
        public string RelationshipType { get; set; }
    }

    public class GuestComplaint
    {
        public string RecordType { get; set; }
        public string DepartmentName { get; set; }
        public string ReservationNumber { get; set; }
        public string RegistrationNumber { get; set; }
        public string RoomNumber { get; set; }
        public string ServiceRequest { get; set; }
        public string ServiceComment { get; set; }
        public string ServiceDate { get; set; }
        public string SLA { get; set; }
        public string ActualTime { get; set; }
        public string StarRating { get; set; }
        public string FeedBack { get; set; }
    }

    public class GuestCreditCard
    {
        public string CreditCardType { get; set; }
        public string CreditCardNumber { get; set; }
        public string RecordType { get; set; }
        public string BankName { get; set; }
        public string ExpiryDate { get; set; }
    }

    public class GuestDependant
    {
        public string DependantType { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string FrequentFlyer { get; set; }
        public string LoyaltyNumber { get; set; }
        public string CreditCardType { get; set; }
        public string CreditCardNumber { get; set; }
        public string RecordType { get; set; }
    }

    public class DHResponseModel
    {
        public string STATUS { get; set; }
        public string MESSAGE { get; set; }
    }
}
