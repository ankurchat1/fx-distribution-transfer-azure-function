﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace funCentralGuestHistory
{
    public class OutputModel
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public object RequestData { get; set; }
    }

    public enum OutputType
    {
        SuccessMessage = 1,
        DynamicMessage = 2,
        DataTableMessage = 3,
        ListMessage = 4
    }

    public class OutputHandler
    {
        public static void OutputSuccessResponse<T>(object model, OutputType Type, ref OutputModel outputModel)
        {
            var _CheckObjectType = model.GetType();

            if (Type == OutputType.SuccessMessage)
            {
                outputModel.Status = 1;
                outputModel.RequestData = FXCloudOutputMessage.SUCCESS;
                outputModel.Message = FXCloudOutputMessage.SUCCESS;
            }
            else if (Type == OutputType.DynamicMessage)
            {
                outputModel.Status = 0;
                outputModel.RequestData = model.ToString();
                outputModel.Message = model.ToString();
            }
            else if (Type == OutputType.DataTableMessage)
            {
                var _SqlResponse = model as DataTable;
                if (_SqlResponse.Rows.Count > 0)
                {
                    if (_SqlResponse.Rows[0][0].ToString() == "0")
                    {
                        outputModel.Status = 1;
                        outputModel.RequestData = _SqlResponse.Rows[0][1].ToString();
                        outputModel.Message = FXCloudOutputMessage.SUCCESS;
                    }
                    else
                    {
                        outputModel.Status = 0;
                        outputModel.RequestData = _SqlResponse.Rows[0][1].ToString();
                        outputModel.Message = FXCloudOutputMessage.ADMINISTRTOR_ERROR;
                    }
                }
            }
            else if (Type == OutputType.ListMessage)
            {
                outputModel.Status = 2;
                outputModel.Message = FXCloudOutputMessage.NO_RECORD_FOUND;
                outputModel.RequestData = outputModel.Message;
                var _SqlResponse = model as List<T>;
                if (_SqlResponse != null)
                {
                    if (_SqlResponse.Count > 0)
                    {
                        outputModel.Status = 1;
                        outputModel.Message = FXCloudOutputMessage.SUCCESS + " - " + $"Total {_SqlResponse.Count} Record Found(s)";
                        outputModel.RequestData = _SqlResponse;
                    }
                }
            }
        }
        public static void OutputErrorResponse(Exception ex, ref OutputModel outputModel)
        {
            outputModel.Status = 0;
            outputModel.RequestData = ex.Message.ToString();
            outputModel.Message = FXCloudOutputMessage.ADMINISTRTOR_ERROR;
        }

    }


    public class FXCloudOutputMessage
    {
        public const int SUCCESS_CODE = 1;
        public const int ERROR_CODE = 0;
        public const string ADMINISTRTOR_ERROR = "Something goes wrong here. Please contact to administrator.";
        public const string SUCCESS = "Request executed successfully.";
        public const string NO_RECORD_FOUND = "No record(s) found.";
    }

    public class GroupRequestDataModel
    {
        public string requestData { get; set; }
        public int status { get; set; }
        public string message { get; set; }
    }

    public class Response
    { 
        public string status { get; set; }
        public string message { get; set; }
    }
}
