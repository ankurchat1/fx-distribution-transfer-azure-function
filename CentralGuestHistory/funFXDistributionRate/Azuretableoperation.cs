﻿using funCentralGuestHistory.Model;
using Microsoft.WindowsAzure.StorageClient.Protocol;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace funCentralGuestHistory
{
    public static class Azuretableoperation
    { 
        public  async static Task<string> UpdateCentralGuestStatus(string AzureAccountName, string AzureStorageKey, string AzureTable, dynamic _objeModel)
        {           
            var _result = string.Empty;
            var context = new DynamicTableContext(AzureTable, new Credentials(AzureAccountName, AzureStorageKey));
            _result = context.InsertOrReplace(_objeModel);
            return  await Task.FromResult(_result);
        }


        public async static Task<FXFDCentralGuestData> GetCentalGuestDataMapping(string Azuretable, string AzureAccountname, string Azurekey, string tokennumber)
        {

            var apiResponseModel = new FXFDCentralGuestData();

            var queryString = string.Format("RowKey eq " + "'" + tokennumber + "'");

            var context = new DynamicTableContext(Azuretable, new Credentials(AzureAccountname, Azurekey));

            foreach (KeyValuePair<string, object> keyValuePair in context.ReservationQuery(queryString))
            {

                switch (keyValuePair.Key)
                {
                    case "PartitionKey":
                        apiResponseModel.PartitionKey = Convert.ToString(keyValuePair.Value);
                        break;
                    case "RowKey":
                        apiResponseModel.RowKey = Convert.ToString(keyValuePair.Value);
                        break;
                    case "FXCRSGuestCode":
                        apiResponseModel.FXCRSGuestCode = Convert.ToString(keyValuePair.Value);
                        break;
                    case "ThirdPartyGuestCode":
                        apiResponseModel.ThirdPartyGuestCode = Convert.ToString(keyValuePair.Value);
                        break;
                    case "GuestName":
                        apiResponseModel.GuestName = Convert.ToString(keyValuePair.Value);
                        break;
                    case "PropertyCode":
                        apiResponseModel.PropertyCode = Convert.ToString(keyValuePair.Value);
                        break;
                    case "FXFDMessage":
                        apiResponseModel.FXFDMessage = Convert.ToString(keyValuePair.Value);
                        break;
                    case "ProcessTime":
                        apiResponseModel.ProcessTime = Convert.ToString(keyValuePair.Value);
                        break;
                    case "MessageStatus":
                        apiResponseModel.MessageStatus = Convert.ToString(keyValuePair.Value);
                        break;
                }
            }

            return await Task.FromResult(apiResponseModel);
        }
    }   
}
