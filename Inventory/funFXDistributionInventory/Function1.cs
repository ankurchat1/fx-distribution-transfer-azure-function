using System;
using System.Net.Http;
using System.Text;
using FXDistribution.Models.ServiceBus;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace funFXDistributionInventory
{
    public static class Function1  
    {
        [FunctionName("Function1")]
        public static async void Run([ServiceBusTrigger("inqueue-inventory", Connection = "ServiceEndpoint")]string myQueueItem, ILogger log)
        {
            log.LogInformation($"C# ServiceBus queue trigger function processed message: {myQueueItem}");
            using (HttpClient client = new HttpClient())
            {
                CommonRequestModel Model = new CommonRequestModel();
                Model= JsonConvert.DeserializeObject<CommonRequestModel>(myQueueItem);
                var _HotelPositionAPI = Environment.GetEnvironmentVariable("HotelPositionAPI");


                var _servicebusconnectionstring = Environment.GetEnvironmentVariable("ServiceEndpoint");

                var token = client.PostAsJsonAsync<CommonRequestModel>(_HotelPositionAPI, Model).Result;                
                var details = token.Content.ReadAsStringAsync().Result;

                Response _response = new Response();
                _response = JsonConvert.DeserializeObject<Response>(details);

                DirectQueueResponsibility directQueueResponsibility = new DirectQueueResponsibility();

                //if (_response.status != "1")
                //{

                //    if (Model.MessageRetryCount < 4)
                //    {
                //        await directQueueResponsibility.QueueExists("inqueue-inventory", _servicebusconnectionstring);

                //        var _Queue = new QueueClient(_servicebusconnectionstring, "inqueue-inventory");
                //        Model.MessageRetryCount = Model.MessageRetryCount + 1;

                //        var QueueMessage = JsonConvert.SerializeObject(Model);
                //        var _Message = new Message(Encoding.UTF8.GetBytes(QueueMessage));

                //        await _Queue.SendAsync(_Message);
                //        await _Queue.CloseAsync();

                //    }
                //}
                //else
                //{

                //}


                log.LogInformation($"ServiceBus queue trigger function processed message: {details}");
            };

        }
    }
}
