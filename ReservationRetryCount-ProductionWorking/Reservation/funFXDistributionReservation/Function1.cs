using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using funFXDistributionReservation.ServiceBus.QueueHelper;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace funFXDistributionReservation
{
    public static class ExponentialRetry
    {
        private static int retryCount = 5;

        [FunctionName("Function1")]
        public static async Task Run(
            [ServiceBusTrigger("inqueue-reservation", Connection = "ServiceEndpoint")]Message message,
            string lockToken,
            MessageReceiver MessageReceiver,
            [ServiceBus("inqueue-reservation", Connection = "ServiceEndpoint")] MessageSender sender,
            ILogger log)
        {
            try
            {
                log.LogInformation($"C# ServiceBus queue trigger function processed message sequence #{message.SystemProperties.SequenceNumber}");
                // throw new Exception("Some exception");
                string _outResponse = string.Empty;

                //ServiceBusQueueMessage serviceBusQueueMessageModel = new ServiceBusQueueMessage();
                // We need to call our api

                var _QueueMessaage = Encoding.UTF8.GetString(message.Body);



                using (HttpClient client = new HttpClient())
                {
                    CommonRequestModel Model = new CommonRequestModel();
                    Model = JsonConvert.DeserializeObject<CommonRequestModel>(_QueueMessaage);
                    var _ReservationAPI = Environment.GetEnvironmentVariable("ReservationAPI");
                    var _servicebusconnectionstring = Environment.GetEnvironmentVariable("ServiceEndpoint");

                    var AzureAccountName = Environment.GetEnvironmentVariable("AzureAccountName");
                    var AzureStorageKey = Environment.GetEnvironmentVariable("AzureStorageKey");
                    var AzureStorageTable = Environment.GetEnvironmentVariable("AzureStorageTable");
                    var AzureStorageQueue = Environment.GetEnvironmentVariable("QueueFailedReservation");
                    var AzureStorageConnection = Environment.GetEnvironmentVariable("AzureStorageConnectionString");


                    var token = client.PostAsJsonAsync<CommonRequestModel>(_ReservationAPI, Model).Result;
                    var details = token.Content.ReadAsStringAsync().Result;

                    Response _response = new Response();
                    _response = JsonConvert.DeserializeObject<Response>(details);               

                    if (_response.status != 1 && _response.message != string.Empty)
                    {                       

                        log.LogInformation("Calculating exponential retry");                       
                        // If the message doesn't have a retry-count, set as 0
                        if (!message.UserProperties.ContainsKey("retry-count"))
                        {
                            message.UserProperties["retry-count"] = 0;
                            message.UserProperties["original-SequenceNumber"] = message.SystemProperties.SequenceNumber;
                        }

                        // If there are more retries available
                        if ((int)message.UserProperties["retry-count"] < retryCount)
                        {
                            var retryMessage = message.Clone();
                            var retryCount = (int)message.UserProperties["retry-count"] + 1;
                            var interval = 1 * retryCount;
                            var scheduledTime = DateTime.UtcNow.AddMinutes(interval);

                            retryMessage.UserProperties["retry-count"] = retryCount;
                            await sender.ScheduleMessageAsync(retryMessage, scheduledTime);                          

                            Azuretableoperation azuretableoperation = new Azuretableoperation();
                            RetryMessageCountModel _retryCountRequestLogModel = new RetryMessageCountModel();
                            Model.MessageRetryCount = retryCount;
                            _retryCountRequestLogModel.PartitionKey = Guid.NewGuid().ToString();
                            _retryCountRequestLogModel.RowKey = Guid.NewGuid().ToString();
                            _retryCountRequestLogModel.PmsCode = Model.PmsCode;
                            _retryCountRequestLogModel.TokenNo = Model.RequestToken;
                            _retryCountRequestLogModel.RetryCount = Model.MessageRetryCount;
                            _retryCountRequestLogModel.ErrorMessage = _response.message.ToString();
                            _retryCountRequestLogModel.RetryTimeUTC = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff");
                            _retryCountRequestLogModel.RetryTimeLocal = DateTime.UtcNow.AddHours(5).AddMinutes(30).ToString("yyyy-MM-dd HH:mm:ss.fff");
                            _outResponse = azuretableoperation.ReservationRetryCountLog(AzureAccountName, AzureStorageKey, AzureStorageTable, _retryCountRequestLogModel).Result;

                            log.LogInformation($"Scheduling message retry {retryCount} to wait {interval} seconds and arrive at {scheduledTime}");


                            if (retryCount == 1)
                            {

                                try
                                {

                                    string _QueueName = AzureStorageQueue.ToString();
                                    //await SendMessageToAzureQueue(AzureAccountName, AzureStorageKey, "1212", "Reservation", "9800", _QueueName);

                                    StorageQueueHelper sq = new StorageQueueHelper(AzureStorageConnection, _QueueName);
                                    await sq.AddNewMessage(_retryCountRequestLogModel.PmsCode +"-"+_retryCountRequestLogModel.TokenNo);


                                }
                                catch (Exception ex)
                                {
                                    // OutputHandler.OutputErrorResponse(ex, ref _Output);
                                }
                            }

                        }

                        // If there are no more retries, deadletter the message (note the host.json config that enables this)
                        else
                        {
                            log.LogCritical($"Exhausted all retries for message sequence # {message.UserProperties["original-SequenceNumber"]}");
                            await MessageReceiver.DeadLetterAsync(lockToken, "Exhausted all retries");
                        }

                    }

                }


            }
            catch (Exception ex)
            {
                //log.LogError(ex, ex.Message);
                //log.LogInformation("Calculating exponential retry");
                //Console.WriteLine("Calculating exponential retry");
                //// If the message doesn't have a retry-count, set as 0
                //if (!message.UserProperties.ContainsKey("retry-count"))
                //{
                //    message.UserProperties["retry-count"] = 0;
                //    message.UserProperties["original-SequenceNumber"] = message.SystemProperties.SequenceNumber;
                //}

                //// If there are more retries available
                //if ((int)message.UserProperties["retry-count"] < retryCount)
                //{
                //    var retryMessage = message.Clone();
                //    var retryCount = (int)message.UserProperties["retry-count"] + 1;
                //    var interval = 5 * retryCount;
                //    var scheduledTime = DateTimeOffset.Now.AddSeconds(interval);

                //    retryMessage.UserProperties["retry-count"] = retryCount;
                //    await sender.ScheduleMessageAsync(retryMessage, scheduledTime);
                //    await MessageReceiver.CompleteAsync(lockToken);

                //    log.LogInformation($"Scheduling message retry {retryCount} to wait {interval} seconds and arrive at {scheduledTime.UtcDateTime}");
                //    //Console.WriteLine($"Scheduling message retry {retryCount} to wait {interval} seconds and arrive at {scheduledTime.UtcDateTime}");
                //}

                //// If there are no more retries, deadletter the message (note the host.json config that enables this)
                //else
                //{
                //    log.LogCritical($"Exhausted all retries for message sequence # {message.UserProperties["original-SequenceNumber"]}");
                //    await MessageReceiver.DeadLetterAsync(lockToken, "Exhausted all retries");
                //}
            }
        }

        public async static Task<string> SendMessageToAzureQueue(string storageAccount, string storageKey, string Token,
           string MessageType, string PmsCode, string QueueName)
        {
            var returnUrl = string.Empty;
            //string containerName = Container;
            //string blobName = string.Format("{0}-{1}-{2}.txt", PmsCode, MessageType, Token);

            string method = "PUT";
            string JsonContent = Token;
            int contentLength = Encoding.UTF8.GetByteCount(JsonContent);

            string requestUri = $"https://{storageAccount}.queue.core.windows.net/{QueueName}";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUri);

            string now = DateTime.UtcNow.ToString("R");

            request.Method = method;
            request.ContentType = "text/plain; charset=UTF-8";
            request.ContentLength = contentLength;
            request.Headers.Add("x-ms-date", now);
            request.Headers.Add("x-ms-version", "2015-12-11");

            //request.Headers.Add("x-ms-blob-type", "BlockBlob");
            request.Headers.Add("Authorization", AuthorizationHeader(method, now, request, storageAccount, storageKey, QueueName).Result);

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(Encoding.UTF8.GetBytes(JsonContent), 0, contentLength);
            }
            try
            {
                using (HttpWebResponse resp = (HttpWebResponse)request.GetResponse())
                {
                    returnUrl = requestUri;
                }
            }
            catch (Exception ex)
            {

            }


            return await Task.FromResult(returnUrl);

        }

        private async static Task<string> AuthorizationHeader(string method, string now, HttpWebRequest request, string storageAccount, string storageKey, string queuename)
        {

            string headerResource = $"x-ms-date:{now}\nx-ms-version:2015-12-11";
            string urlResource = string.Format("/{0}/{1}", storageAccount, queuename);
            string stringToSign = $"{method}\n\n\n{request.ContentLength}\n\n{request.ContentType}\n\n\n\n\n\n\n{headerResource}\n{urlResource}";

            HMACSHA256 hmac = new HMACSHA256(Convert.FromBase64String(storageKey));
            string signature = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(stringToSign)));

            String AuthorizationHeader = String.Format("{0} {1}:{2}", "SharedKey", storageAccount, signature);
            return await Task.FromResult(AuthorizationHeader);
        }

    }
}
