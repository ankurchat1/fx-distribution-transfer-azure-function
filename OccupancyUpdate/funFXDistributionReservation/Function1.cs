using System;
using System.Net.Http;
using System.Text;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace funFXDistributionOccupancyUpdate
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static void Run([ServiceBusTrigger("occupancyinventory", Connection = "ServiceEndpoint")]string myQueueItem, ILogger log)   
        {
            log.LogInformation($"C# ServiceBus queue trigger function processed message: {myQueueItem}");
            using (HttpClient client = new HttpClient())
            {
                OccupancyModel Model = new OccupancyModel();
                Model = JsonConvert.DeserializeObject<OccupancyModel>(myQueueItem);

                var _OccupanyUpdateFXCRS = Environment.GetEnvironmentVariable("OccupanyUpdateFXCRS");

                var _servicebusconnectionstring = Environment.GetEnvironmentVariable("ServiceEndpoint");

                
                    var token = client.PostAsJsonAsync<OccupancyModel>(_OccupanyUpdateFXCRS,Model).Result;
                    var details = token.Content.ReadAsStringAsync().Result;

                    Response _response = new Response();
                    _response = JsonConvert.DeserializeObject<Response>(details);

                    log.LogInformation($"ServiceBus queue trigger function processed message: {details}");
                          

                               
            };
        }
    }
}
