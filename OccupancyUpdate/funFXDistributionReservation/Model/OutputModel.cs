﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace funFXDistributionOccupancyUpdate
{
    public class OutputModel
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public object RequestData { get; set; }
    }

    public enum OutputType
    {
        SuccessMessage = 1,
        DynamicMessage = 2,
        DataTableMessage = 3,
        ListMessage = 4
    }    

    public class Response
    { 
        public string status { get; set; }

        public string message { get; set; }
    }
}
