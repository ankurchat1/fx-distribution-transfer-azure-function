﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace funFXDistributionOccupancyUpdate
{
    public class OccpancyRateFxCRSRequest
    {
        public int PmsCustCode { get; set; }
        public string RoomType { get; set; }
        public string RunDate { get; set; }
    }


    public class OccupancyModel
    {
        public List<Occupancydetails> Occupancydetails { get; set; }
    }

    public class Occupancydetails
    {
        public string PmsCustCode { get; set; }
        public string RoomType { get; set; }
        public string RunDate { get; set; }
    }
}
