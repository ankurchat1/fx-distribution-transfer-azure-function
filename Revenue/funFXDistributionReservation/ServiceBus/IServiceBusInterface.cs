﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace funFXDistributionRevenue
{
    public interface IFxDistributionAdd
    {
        Task<OutputModel> Insert(object model);
    }

    public interface IFxDistributionUpdate
    {
        Task<OutputModel> Update(object model);
    }

    public interface IFxDistributionDelete
    {
        Task<OutputModel> Delete(object model);
    }
}
