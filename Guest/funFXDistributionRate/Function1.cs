using System;
using System.Net.Http;
using System.Text;
using FXDistribution.Models.ServiceBus;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace funFXDistributionInventory
{
    public static class Function1  
    {
        [FunctionName("Function1")]
        public static void Run([ServiceBusTrigger("inqueue-guest", Connection = "ServiceEndpoint")]string myQueueItem, ILogger log)
        {
            log.LogInformation($"C# ServiceBus queue trigger function processed message: {myQueueItem}");
            using (HttpClient client = new HttpClient())
            {
                CommonRequestModel Model = new CommonRequestModel();
                Model= JsonConvert.DeserializeObject<CommonRequestModel>(myQueueItem);
                var _GuestMasterAPI = Environment.GetEnvironmentVariable("GuestMasterAPI");


                var _servicebusconnectionstring = Environment.GetEnvironmentVariable("ServiceEndpoint");

                var token = client.PostAsJsonAsync<CommonRequestModel>(_GuestMasterAPI, Model).Result;                
                var details = token.Content.ReadAsStringAsync().Result;

                Response _response = new Response();
                _response = JsonConvert.DeserializeObject<Response>(details);

                DirectQueueResponsibility directQueueResponsibility = new DirectQueueResponsibility();

                
                log.LogInformation($"ServiceBus queue trigger function processed message: {details}");
            };

        }
    }
}
