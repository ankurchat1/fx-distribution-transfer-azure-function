using System;
using System.Net.Http;
using System.Text;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace funFXDistributionReservation
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static void Run([ServiceBusTrigger("inqueue-reservation", Connection = "ServiceEndpoint")]string myQueueItem, ILogger log)
        {
            log.LogInformation($"C# ServiceBus queue trigger function processed message: {myQueueItem}");
            using (HttpClient client = new HttpClient())
            {
                CommonRequestModel Model = new CommonRequestModel();
                Model = JsonConvert.DeserializeObject<CommonRequestModel>(myQueueItem);
                var _ReservationAPI = Environment.GetEnvironmentVariable("ReservationAPI");

                var _servicebusconnectionstring = Environment.GetEnvironmentVariable("ServiceEndpoint");


                var token = client.PostAsJsonAsync<CommonRequestModel>(_ReservationAPI, Model).Result;
                var details = token.Content.ReadAsStringAsync().Result;

                Response _response = new Response();
                _response = JsonConvert.DeserializeObject<Response>(details);

                DirectQueueResponsibility directQueueResponsibility = new DirectQueueResponsibility();


                //if (_response.status != "1")
                //{

                //    if (Model.MessageRetryCount < 4)
                //    {
                //        await directQueueResponsibility.QueueExists("inqueue-reservation", _servicebusconnectionstring);

                //        var _Queue = new QueueClient(_servicebusconnectionstring, "inqueue-reservation");
                //        Model.MessageRetryCount = Model.MessageRetryCount + 1;

                //        var QueueMessage = JsonConvert.SerializeObject(Model);
                //        var _Message = new Message(Encoding.UTF8.GetBytes(QueueMessage));

                //        await _Queue.SendAsync(_Message);
                //        await _Queue.CloseAsync();

                //    }
                //}
                //else
                //{

                //}

                log.LogInformation($"ServiceBus queue trigger function processed message: {details}");
            };
        }
    }
}
