﻿using System;
using System.Collections.Generic;
using System.Text;

namespace funFXDistributionReservation
{
    public class ServiceBusQueueModel : CommonRequestModel
    {
        public string QueueName { get; set; }
        public string ServiceBusConnection { get; set; }
        public string SourceToken { get; set; }
    }
    public class CommonRequestModel
    {
        public string OutgoingMessage { get; set; }
        public long FxcrsId { get; set; }
        public long PmsCode { get; set; }
        public string Interfacecode { get; set; }
        public string SendTo { get; set; }
        public string APIUserName { get; set; }
        public string APIPassword { get; set; }
        public string APIAuthKey { get; set; }
        public string FxDistributionToken { get; set; }
        public string RequestToken { get; set; }
        public string Status { get; set; }
        public string APIResponseMessage { get; set; }
        public int MessageRetryCount { get; set; }
        public bool IsMailSent { get; set; }
        public string MESSAGETYPE { get; set; }
        public string ProcessDatetime { get; set; }
        public string Destination { get; set; }

        public string Origin { get; set; }

        public string CreatedDatetime { get; set; }

        public string MessageExchangeDateTime { get; set; }
        public string MessageRecieveDateTime { get; set; }
        public string CHANNELTYPE { get; set; }


        public CommonRequestModel()
        {
            ProcessDatetime = DateTime.Now.AddHours(5).AddMinutes(30).ToString("yyyy-MM-dd HH:mm:ss.fff");
        }

    }
}
