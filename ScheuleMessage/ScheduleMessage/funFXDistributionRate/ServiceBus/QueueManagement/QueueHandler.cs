﻿

using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Management;
using System;
using System.Threading.Tasks;

namespace funScheduleMessage
{
    public class QueueHandler : IDisposable
    {
        bool disposed = false;
        private string ServiceBusConnectionString = string.Empty;
        #region Free Me
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // handle.Dispose();
                // Free any other managed objects here.
                //
            }

            disposed = true;
        }
        ~QueueHandler()
        {
            Dispose(false);
        }
        #endregion

        public QueueHandler(string ServiceBusConnectionString)
        {
            this.ServiceBusConnectionString = ServiceBusConnectionString;
        }

        public async Task QueueExists(string QueueName)
        {
            var _managementClient = new ManagementClient(ServiceBusConnectionString);
            var _QueueExists = await _managementClient.QueueExistsAsync(QueueName);
            if (!_QueueExists)
            {

                var _QueueDescription = new QueueDescription(QueueName)
                {
                    // The duration of a peek lock; that is, the amount of time that a message is locked from other receivers.
                    LockDuration = TimeSpan.FromSeconds(45),
                    // Size of the Queue. For non-partitioned entity, this would be the size of the queue. 
                    // For partitioned entity, this would be the size of each partition.
                    MaxSizeInMB = 2048,

                    // This value indicates if the queue requires guard against duplicate messages. 
                    // Find out more in DuplicateDetection sample
                    RequiresDuplicateDetection = false,

                    //Since RequiresDuplicateDetection is false, the following need not be specified and will be ignored.
                    //DuplicateDetectionHistoryTimeWindow = TimeSpan.FromMinutes(2),

                    // This indicates whether the queue supports the concept of session.
                    // Find out more in "Session and Workflow Management Features" sample
                    RequiresSession = false,

                    // The default time to live value for the messages
                    // Find out more in "TimeToLive" sample.
                    DefaultMessageTimeToLive = TimeSpan.FromDays(7),

                    // Duration of idle interval after which the queue is automatically deleted. 
                    AutoDeleteOnIdle = TimeSpan.MaxValue,

                    // Decides whether an expired message due to TTL should be dead-letterd
                    // Find out more in "TimeToLive" sample.
                    EnableDeadLetteringOnMessageExpiration = false,

                    // The maximum delivery count of a message before it is dead-lettered
                    // Find out more in "DeadletterQueue" sample
                    MaxDeliveryCount = 8,

                    // Creating only one partition. 
                    // Find out more in PartitionedQueues sample.
                    EnablePartitioning = false
                };

                await _managementClient.CreateQueueAsync(_QueueDescription);
            }
        }


        public async Task TopicSubscriptionExists(string TopicName, string SubscriptionName)
        {
            var _managementClient = new ManagementClient(ServiceBusConnectionString);
            var _IsExists = await _managementClient.SubscriptionExistsAsync(TopicName, SubscriptionName);
            if (!_IsExists)
            {
                var _Description = new SubscriptionDescription(TopicName, SubscriptionName)
                {
                    LockDuration = TimeSpan.FromSeconds(45),
                    RequiresSession = false,
                    DefaultMessageTimeToLive = TimeSpan.FromDays(7),
                    AutoDeleteOnIdle = TimeSpan.MaxValue,
                    EnableDeadLetteringOnMessageExpiration = false,
                    MaxDeliveryCount = 8,
                };

                var _RoleDescription = new RuleDescription(SubscriptionName, new SqlFilter("sys.To='" + SubscriptionName + "'"));
                await _managementClient.CreateSubscriptionAsync(_Description, _RoleDescription);
            }
        }
    }
}
