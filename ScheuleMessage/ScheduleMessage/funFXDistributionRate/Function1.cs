using System;
using System.Net.Http;
using System.Text;
using FXDistribution.Models.ServiceBus;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace funScheduleMessage
{
    public static class Function1  
    {
        [FunctionName("Function1")]
        public static void Run([ServiceBusTrigger("scheduledmessages", Connection = "ServiceEndpoint")]string myQueueItem, ILogger log)
        {
            log.LogInformation($"C# ServiceBus queue trigger function processed message: {myQueueItem}");
            using (HttpClient client = new HttpClient())
            {
                ScheduleMessageModel Model = new ScheduleMessageModel();
                Model= JsonConvert.DeserializeObject<ScheduleMessageModel>(myQueueItem);
                var _ScheduleMessageAPI = Environment.GetEnvironmentVariable("ScheduleMessageAPI");


                var _servicebusconnectionstring = Environment.GetEnvironmentVariable("ServiceEndpoint");

                var token = client.PostAsJsonAsync<ScheduleMessageModel>(_ScheduleMessageAPI, Model).Result;                
                var details = token.Content.ReadAsStringAsync().Result;

                ScheduleMessageModel _response = new ScheduleMessageModel();
                _response = JsonConvert.DeserializeObject<ScheduleMessageModel>(details);

                DirectQueueResponsibility directQueueResponsibility = new DirectQueueResponsibility();

                
                log.LogInformation($"ServiceBus queue trigger function processed message: {details}");
            };

        }
    }
}
