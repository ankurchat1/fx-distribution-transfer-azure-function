using System;
using System.Net.Http;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace FunctionFxDistReservation
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static void Run([ServiceBusTrigger("inqueue-reservation", Connection = "ServiceEndpoint", IsSessionsEnabled = true)]string myQueueItem, ILogger log)
        {
            log.LogInformation($"C# ServiceBus queue trigger function processed message: {myQueueItem}");
            using (HttpClient client = new HttpClient())
            {
                CommonRequestModel Model = new CommonRequestModel();
                Model = JsonConvert.DeserializeObject<CommonRequestModel>(myQueueItem);
                var _ReservationAPI = Environment.GetEnvironmentVariable("ReservationAPI");

                var _servicebusconnectionstring = Environment.GetEnvironmentVariable("ServiceEndpoint");


                var token = client.PostAsJsonAsync<CommonRequestModel>(_ReservationAPI, Model).Result;
                var details = token.Content.ReadAsStringAsync().Result;

                Response _response = new Response();
                _response = JsonConvert.DeserializeObject<Response>(details);

                DirectQueueResponsibility directQueueResponsibility = new DirectQueueResponsibility();


                log.LogInformation($"ServiceBus queue trigger function processed message: {details}");
            };
        }
    }
}
