﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using System.Text;
using Newtonsoft.Json;

namespace FunctionFxDistReservation
{
    public class DirectQueueResponsibility : IDisposable
    {
        bool disposed = false;

        #region Free Me
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // handle.Dispose();
                // Free any other managed objects here.
                //
            }

            disposed = true;
        }
        ~DirectQueueResponsibility()
        {
            Dispose(false);
        }
        #endregion

        public async Task<OutputModel> DirectInsertToQueue(ServiceBusQueueModel QueueModel)
        {
            var _Output = new OutputModel();
            CommonRequestModel _RequestModel = new CommonRequestModel();
            _RequestModel.Interfacecode = QueueModel.Interfacecode;
            _RequestModel.OutgoingMessage = QueueModel.OutgoingMessage;
            _RequestModel.APIUserName = QueueModel.APIUserName;
            _RequestModel.APIPassword = QueueModel.APIPassword;
            _RequestModel.PmsCode = QueueModel.PmsCode;
            _RequestModel.FxcrsId = QueueModel.FxcrsId;
            _RequestModel.SendTo = QueueModel.SendTo;
            _RequestModel.APIAuthKey = QueueModel.APIAuthKey;
            _RequestModel.FxDistributionToken = Guid.NewGuid().ToString();
            _RequestModel.MESSAGETYPE = QueueModel.MESSAGETYPE;
            _RequestModel.Status = "Inprogress";
            _RequestModel.RequestToken = QueueModel.RequestToken;
            _RequestModel.Destination = QueueModel.Destination;

            var QueueMessage = JsonConvert.SerializeObject(_RequestModel);   ; //JsonSerializer.Serialize(_RequestModel);

            try
            {
                // Check Queue Name Exists or not 
                await QueueExists(QueueModel.QueueName, QueueModel.ServiceBusConnection);              
                var _Queue = new QueueClient(QueueModel.ServiceBusConnection, QueueModel.QueueName);
                var _Message = new Message(Encoding.UTF8.GetBytes(QueueMessage));
                
                await _Queue.SendAsync(_Message);
                await _Queue.CloseAsync();
                OutputHandler.OutputSuccessResponse<string>(null, OutputType.SuccessMessage, ref _Output);
            }
            catch (Exception ex)
            {
                OutputHandler.OutputErrorResponse(ex, ref _Output);
            }
            return await Task.FromResult(_Output);
        }


        public async Task QueueExists(string QueueName, string ServiceBusConnectionString)
        {
            using (var _QueueHandler = new QueueHandler(ServiceBusConnectionString))
            {
                await _QueueHandler.QueueExists(QueueName);
            }
        }
    }
}
