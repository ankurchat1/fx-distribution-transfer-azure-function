﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FXDistribution.Models.ServiceBus;

namespace funCompanyBulk
{
    public class DirectQueueConcrete : IFxDistributionAdd
    {
        public async Task<OutputModel> Insert(object model)
        {
            var _Output = new OutputModel();
            var _Model = model as ServiceBusQueueModel;
            using (var _QueueResponse = new DirectQueueResponsibility())
            {
                _Output = await _QueueResponse.DirectInsertToQueue(_Model);
            }
            return await Task.FromResult(_Output);
        }
    }
}
