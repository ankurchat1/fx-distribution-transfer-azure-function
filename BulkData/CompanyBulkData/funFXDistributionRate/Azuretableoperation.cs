﻿using Microsoft.WindowsAzure.StorageClient.Protocol;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace funCompanyBulk
{
    public class Azuretableoperation
    {
        public Task<string> PmsCompanyDataLog(string AzureAccountName, string AzureStorageKey, string AzureTable, PMSDataCompanyRequest pMSDataCompanyRequest)// List<PMSDataCompanyRequest> pMSDataCompanyRequest)
        {


            var UploadedbolbURL = string.Empty;


            var incommingModelRequest = JsonConvert.SerializeObject(pMSDataCompanyRequest.XmlMessages);

            UploadedbolbURL = BlobStorage.UploadBlobWithRestAPI(AzureAccountName, AzureStorageKey,
               Guid.NewGuid().ToString(), "", pMSDataCompanyRequest.PropertyCode.ToString(), "fxdist-bulkdata", incommingModelRequest).Result;

            pMSDataCompanyRequest.XmlMessages = UploadedbolbURL;

            var _result = string.Empty;
            var context = new DynamicTableContext(AzureTable, new Credentials(AzureAccountName, AzureStorageKey));
            _result = context.Insert(pMSDataCompanyRequest);
            return Task.FromResult(_result);
        }


        public  Task<string> UpdatePmsCompanyDataLog(string AzureAccountName, string AzureStorageKey, string AzureTable, PMSDataCompanyRequest pMSDataCompanyRequest)
        {
            var incommingModelRequest = JsonConvert.SerializeObject(pMSDataCompanyRequest.XmlMessages);
            var _result = string.Empty;
            var context = new DynamicTableContext(AzureTable, new Credentials(AzureAccountName, AzureStorageKey));
            _result = context.InsertOrReplace(pMSDataCompanyRequest);
            return  Task.FromResult(_result);
        }
    }

    public class PMSDataCompanyRequest
    {
        public string PartitionKey { get; set; } = "9109";
        public string RowKey { get; set; } = Guid.NewGuid().ToString();
        public long SerialNo { get; set; }
        public long PropertyCode { get; set; }
        public string ComCode { get; set; }
        public string XmlMessages { get; set; }
        public string ProcessTime { get; set; }
        public string MessageStatus { get; set; }
    }
}
