using System;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Text;
using CompanyBulk.Model;
using FXDistribution.Models.ServiceBus;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace funCompanyBulk
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static void Run([ServiceBusTrigger("inqueue-company-bulk", Connection = "ServiceEndpoint")]string myQueueItem, ILogger log)
        {
            //log.LogInformation($"C# ServiceBus queue trigger function processed message: {myQueueItem}");
            using (HttpClient client = new HttpClient())
            {
                  ServiceBusQueueMessage Model = new ServiceBusQueueMessage();
                Model = JsonConvert.DeserializeObject<ServiceBusQueueMessage>(myQueueItem);
                var _CompanyBulkAPI = Environment.GetEnvironmentVariable("CompanyBulkAPI");
                var _servicebusconnectionstring = Environment.GetEnvironmentVariable("ServiceEndpoint");

                var AzureAccountName = Environment.GetEnvironmentVariable("AzureAccountName");
                var AzureStorageKey = Environment.GetEnvironmentVariable("AzureStorageKey");
                var AzureStorageTable = Environment.GetEnvironmentVariable("AzureStorageTable");

                

                string _outResponse = string.Empty;

                //foreach (var dr in Model.XMLMessageUrl)
                //{
                string url = string.Empty;
                string content = string.Empty;
                string message = string.Empty;
                BulkRequestCompany incomming = new BulkRequestCompany();
                                                                                                                                                 
                //url = Convert.ToString(dr["XMLMessages"]);
                content = new WebClient().DownloadString(Model.XMLMessageUrl);
                content = JsonConvert.DeserializeObject<string>(content);
                incomming.Data = content;
                message = JsonConvert.SerializeObject(incomming);
                Azuretableoperation azuretableoperation = new Azuretableoperation();
                PMSDataCompanyRequest item = new PMSDataCompanyRequest();
                item.PartitionKey = Model.PartitionKey;
                item.RowKey = Model.RowKey;


                if (content != null)
                {

                    try
                    {
                        var token = client.PostAsJsonAsync<BulkRequestCompany>(_CompanyBulkAPI, incomming).Result;
                        var details = token.Content.ReadAsStringAsync().Result;

                        Response _response = new Response();
                        _response = JsonConvert.DeserializeObject<Response>(details);

                        item.MessageStatus = details.ToString();
                    }
                    catch (Exception ex)
                    {

                        
                    }

                    


                }

                //}

                
                //_outResponse = azuretableoperation.UpdatePmsCompanyDataLog(AzureAccountName, AzureStorageKey, AzureStorageTable, item).Result;





                //log.LogInformation($"ServiceBus queue trigger function processed message: {details}");
            };

        }
    }

    public class BulkRequestCompany
    {
        public string Data { get; set; }
    }
}
