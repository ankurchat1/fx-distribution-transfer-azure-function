﻿using System;
using System.IO;
using System.Text;
using System.Net;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace funCompanyBulk
{
    public static class BlobStorage
    {
        public async static Task<string> UploadBlobWithRestAPI(string storageAccount, string storageKey,string Token,
            string MessageType,string PmsCode,string Container,string JsonData)
        {
            var returnUrl = string.Empty;
            string containerName = Container;
            string blobName = string.Format( "{0}-{1}-{2}.txt",PmsCode,MessageType,Token);

            string method = "PUT";
            string JsonContent = JsonData;
            int contentLength = Encoding.UTF8.GetByteCount(JsonContent);

            string requestUri = $"https://{storageAccount}.blob.core.windows.net/{containerName}/{blobName}";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUri);

            string now = DateTime.UtcNow.ToString("R");

            request.Method = method;
            request.ContentType = "text/plain; charset=UTF-8";
            request.ContentLength = contentLength;

            request.Headers.Add("x-ms-version", "2015-12-11");
            request.Headers.Add("x-ms-date", now);
            request.Headers.Add("x-ms-blob-type", "BlockBlob");
            request.Headers.Add("Authorization",  AuthorizationHeader(method, now, request, storageAccount, storageKey, containerName, blobName).Result);

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(Encoding.UTF8.GetBytes(JsonContent), 0, contentLength);
            }

            using (HttpWebResponse resp = (HttpWebResponse)request.GetResponse())
            {                
                returnUrl = requestUri;               
            }

            return await Task.FromResult(returnUrl);

        }

        public async static Task<string> AuthorizationHeader(string method, string now, HttpWebRequest request, string storageAccount, string storageKey, string containerName, string blobName)
        {

            string headerResource = $"x-ms-blob-type:BlockBlob\nx-ms-date:{now}\nx-ms-version:2015-12-11";
            string urlResource = $"/{storageAccount}/{containerName}/{blobName}";
            string stringToSign = $"{method}\n\n\n{request.ContentLength}\n\n{request.ContentType}\n\n\n\n\n\n\n{headerResource}\n{urlResource}";

            HMACSHA256 hmac = new HMACSHA256(Convert.FromBase64String(storageKey));
            string signature = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(stringToSign)));

            String AuthorizationHeader = String.Format("{0} {1}:{2}", "SharedKey", storageAccount, signature);            
            return await Task.FromResult(AuthorizationHeader);
        }
    }
}
