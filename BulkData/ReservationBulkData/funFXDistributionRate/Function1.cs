using System;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Text;
using CompanyBulk.Model;
using FXDistribution.Models.ServiceBus;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace funCompanyBulk
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static void Run([ServiceBusTrigger("inqueue-reservation-bulk", Connection = "ServiceEndpoint")]string myQueueItem, ILogger log)
        {
            //log.LogInformation($"C# ServiceBus queue trigger function processed message: {myQueueItem}");
            using (HttpClient client = new HttpClient())
            {
                  ServiceBusQueueMessage Model = new ServiceBusQueueMessage();

                Model = JsonConvert.DeserializeObject<ServiceBusQueueMessage>(myQueueItem);
                var _ReservationBulkAPIAPI = Environment.GetEnvironmentVariable("ReservationBulkAPI");
                var _servicebusconnectionstring = Environment.GetEnvironmentVariable("ServiceEndpoint");

                var AzureAccountName = Environment.GetEnvironmentVariable("AzureAccountName");
                var AzureStorageKey = Environment.GetEnvironmentVariable("AzureStorageKey");
                var AzureStorageTable = Environment.GetEnvironmentVariable("AzureStorageTable");

                

                string _outResponse = string.Empty;

                //foreach (var dr in Model.XMLMessageUrl)
                //{
                string url = string.Empty;
                string content = string.Empty;
                string message = string.Empty;
                IncommingModel incomming = new IncommingModel();
                                                                                                                                                 
                //url = Convert.ToString(dr["XMLMessages"]);
                content = new WebClient().DownloadString(Model.XMLMessageUrl);
                content = JsonConvert.DeserializeObject<string>(content);
                incomming.INCOMINGMESSAGE = content;
                incomming.PMSCUSTCODE = 9800;
                incomming.PMSCUSTID = 0;
                incomming.DATASOURCE = "DATAHUB";
                incomming.DATADESTINATION = "FXFD";
                incomming.RECEIVEDDATE = DateTime.Now.ToString();
                incomming.MESSAGETYPE = "Reservation";
                incomming.TOKENNUMBER = Guid.NewGuid().ToString();
                incomming.CREATEDDATE = DateTime.Now.ToString();
                incomming.CHANNELTYPE = "";

                message = JsonConvert.SerializeObject(incomming);
                Azuretableoperation azuretableoperation = new Azuretableoperation();
                PMSDataCompanyRequest item = new PMSDataCompanyRequest();
                item.PartitionKey = Model.PartitionKey;
                item.RowKey = Model.RowKey;


                if (content != null)
                {

                    try
                    {
                        var token = client.PostAsJsonAsync<IncommingModel>(_ReservationBulkAPIAPI, incomming).Result;
                        var details = token.Content.ReadAsStringAsync().Result;

                        Response _response = new Response();
                        _response = JsonConvert.DeserializeObject<Response>(details);

                        item.MessageStatus = details.ToString();
                    }
                    catch (Exception ex)
                    {

                        
                    }

                    


                }

                //}

                
                _outResponse = azuretableoperation.UpdatePmsCompanyDataLog(AzureAccountName, AzureStorageKey, AzureStorageTable, item).Result;





                //log.LogInformation($"ServiceBus queue trigger function processed message: {details}");
            };

        }
    }

    public class IncommingModel
    {
        /// <summary>
        /// Customer id ,which is unique
        /// </summary>
        public long PMSCUSTCODE { get; set; }
        /// <summary>
        /// Fxcrs id , whcih is unique created for the property in Fxcrs
        /// </summary>
        public long PMSCUSTID { get; set; }
        /// <summary>
        /// From where the message has generated
        /// </summary>
        public string DATASOURCE { get; set; }
        /// <summary>
        /// Where to send the message FXFD/Datahub or other interface
        /// </summary>
        public string DATADESTINATION { get; set; }
        /// <summary>
        /// Created Date
        /// </summary>
        public string RECEIVEDDATE { get; set; }
        /// <summary>
        /// What kind of incomming message ex:Reservation/Rate etc
        /// </summary>
        public string MESSAGETYPE { get; set; }
        /// <summary>
        /// Unique Identitification 
        /// </summary>
        public string TOKENNUMBER { get; set; }
        /// <summary>
        /// Bundle of Data XML OR JSON OR any string 
        /// </summary>
        public string INCOMINGMESSAGE { get; set; }
        /// <summary>
        /// Create date once the
        /// </summary>
        public string CREATEDDATE { get; set; }

        public string CHANNELTYPE { get; set; }

        public IncommingModel()
        {
            CREATEDDATE = DateTime.Now.AddHours(5).AddDays(30).ToString("yyyy-MM-ddTHH:mm:ss.fff");
        }

    }
}
