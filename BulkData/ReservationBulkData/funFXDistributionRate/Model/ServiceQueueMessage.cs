﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompanyBulk.Model
{
    public class ServiceBusQueueMessage
    {
        public string PartitionKey { get; set; } = Guid.NewGuid().ToString();
        public string RowKey { get; set; } = Guid.NewGuid().ToString();
        public string XMLMessageUrl { get; set; }

    }
}
